var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
$(document).ready(function () {
    // Gọi API để lấy danh sách khóa học
    $.get("https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses", function (courses) {
        // Lọc danh sách các khóa học popular
        var popularCourses = courses.filter(function (course) {
            return course.isPopular;
        });

        // Lấy tối đa 4 khóa học popular
        popularCourses = popularCourses.slice(0, 4);

        // Hiển thị danh sách khóa học popular
        displayCourses(popularCourses, "#popular-courses");

        // Lọc danh sách các khóa học trending
        var trendingCourses = courses.filter(function (course) {
            return course.isTrending;
        });

        // Lấy tối đa 4 khóa học trending
        trendingCourses = trendingCourses.slice(0, 4);

        // Hiển thị danh sách khóa học trending
        displayCourses(trendingCourses, "#trending-courses");
    });
});

// Hàm hiển thị danh sách khóa học
function displayCourses(courses, containerId) {
    var container = $(containerId);

    // Xóa các khóa học cũ trong vùng hiển thị
    container.empty();

    // Duyệt qua danh sách khóa học và tạo HTML hiển thị
    courses.forEach(function (course) {
        var courseHtml = `
      <div class="col-md-3 angular-content">
        <div class="image-angular">
          <img src="${course.coverImage}" alt="" width="283.860px" height="150px" >
        </div>
        <div class="course-description">
          <b>${course.courseName}</b>
        </div>
        <div class="course-middle-content">
          <i class="far fa-clock">&nbsp;${course.duration}</i>
          <a>${course.level}</a>
        </div>
        <div class="price-course">
          <b>${course.discountPrice}</b>
          <a><s>${course.price}</s></a>
        </div>
        <div class="line"></div>
        <div class="footer-course-content">
          <img src="${course.teacherPhoto}" alt="${course.teacherName}">
          <div class="teacher-name">${course.teacherName}</div>
          <i class="far fa-bookmark"></i>
        </div>
      </div>
    `;

        // Thêm HTML của khóa học vào vùng hiển thị
        container.append(courseHtml);
    });
}